<?php
/*
Template Name: Old Slider
*/
?>
<?php get_header('oldslider'); ?>
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'page' ); ?>    
  </div><!--row-->
</div><!--content-->
</div><!--container-->
<section class="buttons"><div class="container"><?php get_template_part( 'featuredbuttons' ); ?></div></section>
<div class="container">
<?php get_footer(); ?>