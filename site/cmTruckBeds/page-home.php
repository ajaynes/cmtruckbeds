<?php
/*
Template Name: Home Page
*/
?>
<?php get_header('home'); ?>
<?php get_template_part( 'truckSlider' ); ?>
<div class="container">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'home' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>