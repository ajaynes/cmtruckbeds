<?php date_default_timezone_set('CST'); ?>
<div id="slider" class="carousel slide">
  <div class="carousel-inner">
      <div class="item active">
          <?php if ( of_get_option('slide1') ){
                  if (of_get_option('slidelink1')){
                  echo '<a href="'; echo of_get_option('slidelink1');
                    if (of_get_option('linktarget1')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide1'); echo '" alt="'; echo of_get_option('alt1'); echo '" />'; 
                  if (of_get_option('slidelink1')){echo '</a>';}
					}
					else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/utility-trailers.jpg" alt="Utility Trailer" />';
              }?>
      </div>
      <!--item-->
      
      <div class="item">
            <?php if ( of_get_option('slide2') ){
                  if (of_get_option('slidelink2')){
                  echo '<a href="'; echo of_get_option('slidelink2');
                    if (of_get_option('linktarget2')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide2'); echo '" alt="'; echo of_get_option('alt2'); echo '" />'; 
                  if (of_get_option('slidelink2')){echo '</a>';}
						}
						else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/dump-trailer.jpg" alt="Dump Trailer" />';
              }?>
      </div>
      <!--item-->
      
      <div class="item">
          <?php if ( of_get_option('slide3') ){ 
                  if (of_get_option('slidelink3')){
                  echo '<a href="'; echo of_get_option('slidelink3');
                    if (of_get_option('linktarget3')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide3'); echo '" alt="'; echo of_get_option('alt3'); echo '" />'; 
                  if (of_get_option('slidelink3')){echo '</a>';}
					}
					else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/gooseneck-trailers.jpg" alt="Gooseneck Trailers" />';
              }?>
      </div>
      <!--item-->
      
      <?php if ( of_get_option('slide4')) if (date("m/d/Y") >= of_get_option('start4')) if (date("m/d/Y") < of_get_option('end4')) {{ ?>
          <div class="item">
               <?php if ( of_get_option('slide4') ) { 
                  if (of_get_option('slidelink4')){
                  echo '<a href="'; echo of_get_option('slidelink4');
                    if (of_get_option('linktarget4')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide4'); echo '" alt="'; echo of_get_option('alt4'); echo '" />'; 
                  if (of_get_option('slidelink4')){echo '</a>';}
              }}?>
          </div>
      <?php }?>
      <!--item-->
      
      <?php if ( of_get_option('slide5')) if (date("m/d/Y") >= of_get_option('start5')) if (date("m/d/Y") < of_get_option('end5')) {{ ?>
          <div class="item">
               <?php if ( of_get_option('slide5') ) { 
                  if (of_get_option('slidelink5')){
                  echo '<a href="'; echo of_get_option('slidelink5');
                    if (of_get_option('linktarget5')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide5'); echo '" alt="'; echo of_get_option('alt5'); echo '" />'; 
                  if (of_get_option('slidelink5')){echo '</a>';}
              }}?>
          </div>
      <?php }?>
      <!--item-->
      
      <?php if ( of_get_option('slide6')) if (date("m/d/Y") >= of_get_option('start6')) if (date("m/d/Y") < of_get_option('end6')) {{ ?>
          <div class="item">
              <?php if ( of_get_option('slide6') ) { 
                  if (of_get_option('slidelink6')){
                  echo '<a href="'; echo of_get_option('slidelink6');
                    if (of_get_option('linktarget6')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide6'); echo '" alt="'; echo of_get_option('alt6'); echo '" />'; 
                  if (of_get_option('slidelink6')){echo '</a>';}
              }}?>
          </div>
      <?php }?>
      <!--item--> 
  </div><!--carousel inner-->   
</div><!--slider-->