<div class="col-xs-12 col-sm-6">
    <aside>
        <?php if ( is_active_sidebar( 'home' ) ) : ?>
            <?php dynamic_sidebar( 'home' ); ?>
        <?php endif; ?>
    </aside>
</div>