jQuery(document).ready(function () {
	jQuery("footer li.facebook a, footer li.google a, footer li.twitter a, footer li.youtube a, .connectbox li.facebook a, .connectbox li.google a, .connectbox li.twitter a, .connectbox li.youtube a, .connectboxhome li.facebook a, .connectboxhome li.google a, .connectboxhome li.twitter a, .connectboxhome li.youtube a").each(function () {
		jQuery(this).html(jQuery(this).text(""))
	}), jQuery("footer .google a").append('<span class="fa-stack fa-lg"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-google-plus fa-stack-1x fa-inverse"></span></span>'), jQuery("footer .facebook a").append('<span class="fa-stack fa-lg"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-facebook fa-stack-1x fa-inverse"></span></span>'), jQuery("footer .twitter a").append('<span class="fa-stack fa-lg"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-twitter fa-stack-1x fa-inverse"></span></span>'), jQuery("footer .youtube a").append('<span class="fa-stack fa-lg"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-youtube fa-stack-1x fa-inverse"></span></span>'), jQuery(".connectboxhome .google a, .connectbox .google a").append('<span class="fa-stack"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-google-plus fa-stack-1x fa-inverse"></span></span>'), jQuery(".connectboxhome .facebook a, .connectbox .facebook a").append('<span class="fa-stack"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-facebook fa-stack-1x fa-inverse"></span></span>'), jQuery(".connectboxhome .twitter a,  .connectbox .twitter a").append('<span class="fa-stack"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-twitter fa-stack-1x fa-inverse"></span></span>'), jQuery(".connectboxhome .youtube a, .connectbox .youtube a").append('<span class="fa-stack"><span class="fa fa-square fa-stack-2x"></span><span class="fa fa-youtube-play fa-stack-1x fa-inverse"></span></span>'), jQuery(".carousel-caption:not(:has(h4))").hide(), jQuery("*").each(function () {
		jQuery(this).removeAttr("align")
	}), jQuery(window).scroll(function () {
		jQuery(this).scrollTop() > 200 ? jQuery(".go-top").fadeIn(200) : jQuery(".go-top").fadeOut(200)
	}), jQuery(".go-top").click(function (a) {
		a.preventDefault(), jQuery("html, body").animate({
			scrollTop: 0
		}, 300)
	}), jQuery(".gfield_contains_required.hidden_label .ginput_container label").append('<span class="gfield_required">*</span>'), jQuery(".gform_button").addClass("btn btn-lg btn-block btn-primary"), jQuery("#fsSubmitButton2273941").addClass("btn btn-lg btn-info pull-right bt"), jQuery("#trailer").carousel(), jQuery(".gv-table-container.gv-container table").addClass("table table-striped"), jQuery(".gv-table-container.gv-container table").wrap('<div class="table-responsive"></div>'), jQuery(".magnific").magnificPopup({
		type: "image"
	}), jQuery(".singletruckbed:last").css("margin-bottom", 20), jQuery("#accordion").on("shown.bs.collapse", function () {
		var a = jQuery(this).find(".collapse.in");
		jQuery("html, body").animate({
			scrollTop: jQuery(a).offset().top
		}, 500)
	}), jQuery(".panel-heading").on("click", function (a) {
		jQuery(this).parents(".panel").children(".panel-collapse").hasClass("in") && a.stopPropagation()
	}), jQuery(".fsSubmit.fsPagination input, .fsNextButton, .fsPagination .fsPreviousButton").addClass("btn btn-lg btn-primary");
	var a = jQuery(".featuredButtons .button").map(function () {
			return jQuery(this).height()
		}).get(),
		b = Math.max.apply(null, a);
	jQuery(".featuredButtons .button").height(b + 35), jQuery("#fsSubmitButton2273941").removeClass("btn-primary")
}), jQuery(document).ready(function (a) {
	window.location.hash && (window.location = window.location.replace("#", ""))
});
$("#slideshow > div:gt(0)").hide();

setInterval(function() { 
  $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
},  3000);