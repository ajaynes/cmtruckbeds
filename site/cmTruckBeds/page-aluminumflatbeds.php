<?php
/*
Template Name: Aluminum Flat Beds
*/
?>
<?php get_header(); ?>
<div class="content">
<div class="container"
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'aluminumflatbeds' ); ?>
  </div><!--row-->
</div><!--content-->
</div><!--container-->
<section class="buttons"><div class="container"><?php get_template_part( 'featuredbuttons' ); ?></div></section>
<div class="container">
<?php get_footer(); ?>