<div class="featuredButtons">
	<div class="row">
  	<div class="col-xs-12 col-sm-4">
    	<a id="brochure" class="button" href="/request-a-brochure/">
      	<h3>Request a Brochure</h3>
        <p>Learn more about the ins and outs of our CM Truck Beds. With our brochures, your questions about different models and styles will be answered.</p>
      </a>
    </div><!--col-xs-12 col-sm-4-->
    <div class="col-xs-12 col-sm-4">
    	<a id="resource" class="button" href="/resources/">
      	<h3>CM Resource Center</h3>
        <p>Looking for additional information about our owners' manual, truck bed tips, or our warranty? Our resource center will have what you are searching for.</p>	
      </a>
    </div><!--col-xs-12 col-sm-4-->
    <div class="col-xs-12 col-sm-4">
    	<a id="become" class="button" href="/become-a-distributor/">
      	<h3>Become a Distributor</h3>
        <p>Join the team of one of the nation's most recognized truck beds. At CM, we are always looking for strategically located distributor partners.</p>	
      </a>
    </div><!--col-xs-12 col-sm-4-->
  </div><!--row-->
</div><!--featuredButtons-->