<?php
/*
Template Name: Fullwidth No sidebar
*/
?>
<?php get_header(); ?>
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'fullwidth' ); ?>
  </div><!--row-->
</div><!--content-->
</div><!--container-->
<section class="buttons"><div class="container"><?php get_template_part( 'featuredbuttons' ); ?></div></section>
<div class="container">
<?php get_footer(); ?>