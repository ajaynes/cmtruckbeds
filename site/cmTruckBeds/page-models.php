<?php
/*
Template Name: Truck Bed Models
*/
?>
<?php get_header(); ?>
</div>
<?php get_template_part( '/inc/parts/content', 'models' ); ?>
<div class="container">
<div class="content">
	<div class="row">
    <div class="col-xs-12"><?php the_field('design_features_&_hardware'); ?></div>
    <div class="col-xs-12"><?php the_field('showcase'); ?></div>
    <?php get_template_part( 'disclaimer' ); ?>
  </div><!--row-->
</div><!--content-->
</div><!--container-->
<section class="buttons"><div class="container"><?php get_template_part( 'featuredbuttons' ); ?></div></section>
<div class="container">
<?php get_footer(); ?>