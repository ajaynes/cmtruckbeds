</div>
<!--container-->
<?php get_template_part( 'newsletter' ); ?>
<footer>
  <div class="container">
  	<!--<div class="row">
    	<script type="text/javascript" src="https://bigtextrailers.formstack.com/forms/js.php/cm_tb_newsletter?nojquery=1&nomodernizr=1&no_style_strict=1"></script><noscript><a href="https://bigtextrailers.formstack.com/forms/cm_tb_newsletter" title="Online Form">Online Form - Newsletter Sign up</a></noscript><script type='text/javascript'>if (typeof $ == 'undefined' && jQuery){ $ = jQuery}</script>
    </div>-->
  	<div class="row">
    	<div class="col-sm-3">
      	<?php if ( is_active_sidebar( 'footer1' ) ) : ?>
            <?php dynamic_sidebar( 'footer1' ); ?>
        <?php endif; ?>
      </div><!--col-sm-3-->
      <div class="col-sm-3">
      	<?php if ( is_active_sidebar( 'footer2' ) ) : ?>
            <?php dynamic_sidebar( 'footer2' ); ?>
        <?php endif; ?>
      </div><!--col-sm-3-->
      <div class="col-sm-3">
      	<?php if ( is_active_sidebar( 'footer3' ) ) : ?>
            <?php dynamic_sidebar( 'footer3' ); ?>
        <?php endif; ?>
      </div><!--col-sm-3-->
      <div class="col-sm-3">
      	<?php if ( is_active_sidebar( 'footer4' ) ) : ?>
            <?php dynamic_sidebar( 'footer4' ); ?>
        <?php endif; ?>
      </div><!--col-sm-3-->
    </div><!--row-->
    <div class="row info">
      <div class="col-sm-7">
        <p class="disclaimer">&copy;<?php echo date('Y'); ?> CM Truck Beds. All Rights Reserved. All text, images, logos, content, design, and coding of this Web site is protected by all applicable copyright and trademark laws. <a href="http://cmtruckbeds.com/privacy-policy/">Privacy Policy</a></p>
      </div>
      <!--col-sm-7-->
      <div class="col-sm-5 text-center">
        <?php wp_nav_menu(array(
						'theme_location' => 'social',
						'container' => 'nav',
						'menu_class' => 'list-inline list-unstyled',
						'fallback_cb' => false
					));
				?>
      </div>
      <!--col-xs-5 center--> 
    </div>
    <!--row--> 
  </div>
  <!--container--> 
</footer>
<?php wp_footer(); ?>
</body></html>