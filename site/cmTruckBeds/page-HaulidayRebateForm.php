<?php
/*
Template Name: Holiday Rebate Form
*/
?>
<div class="display-none"><?php get_header(); ?></div> 
<div class="container">
<div class="main">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'holidayrebateform' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>