<!DOCTYPE html>
<html <?php language_attributes(); ?>>
		<!--[if lte IE 8 ]> <html <?php language_attributes(); ?> class="ie8"> <![endif]--><head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
  	<?php get_template_part( 'livechat' ); ?>
    <a href="#" class="go-top hidden-xs hidden-sm"><span class="fa fa-arrow-circle-o-up fa-2x fa-fw"></span></a>
		<header>
    	<div class="container">
        <div class="row">
        	<div class="col-xs-12 col-sm-9 col-sm-offset-3 col-md-7 col-md-offset-5">
        	<?php get_template_part( 'connectBox' ); ?>
          </div> 
        </div><!--row-->
      </div><!--container-->
		</header>
    <section class="navbg">
      <div class="container">
      	<?php get_template_part( '/inc/parts/menu', 'nav' ); ?>
      </div>
    </section>
    <div class="container">