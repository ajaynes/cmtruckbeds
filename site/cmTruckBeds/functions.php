<?php
//testing this out to fix the permalinks issue - AJaynes
function prefix_setup_schedule() {
	if ( ! wp_next_scheduled( 'prefix_hourly_event' ) ) {
		wp_schedule_event( time(), 'hourly', 'prefix_hourly_event');
	}
}
add_action( 'wp', 'prefix_setup_schedule' );

function prefix_do_this_hourly() {
	flush_rewrite_rules( true );
}
add_action( 'prefix_hourly_event', 'prefix_do_this_hourly' );
//end test code
add_filter( 'gform_disable_notification', 'disable_notification', 10, 4 );
function disable_notification( $is_disabled, $notification, $form, $entry ) {

    //There is no concept of admin notifications anymore, so we will need to disable notifications based on other criteria such as name
    if ( $notification['name'] == 'Warranty Notification' ) {
        return true;
    }

    return $is_disabled;
}
add_filter('widget_text', 'do_shortcode');
//Loads the Theme Options Panel 
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';
require_once get_template_directory() . '/options.php';
//custom dashboard widget
add_action('wp_dashboard_setup', 'custom_dashboard_widgets');
function custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_video_widget', 'Recent Website Updates', 'custom_dashboard_video');
}
function custom_dashboard_video() {
	echo of_get_option('updates');
}
add_filter("gform_enable_field_label_visibility_settings", "__return_true");
function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More...', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
function custom_excerpt_length( $length ) {
	return 60;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
//add featured image support to posts
add_theme_support( 'post-thumbnails' );
//featured image links to post
add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );
function my_post_image_html( $html, $post_id, $post_image_id ) {
  $html = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';
  return $html;
}
//add class "img-responsive" to all images that are inserted into pages and posts
function add_image_class($class){
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class','add_image_class');
//Register sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'id' => 'main',
		'name' => 'Main Sidebar',
		'description' => 'This is the main sidebar',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'footer1',
		'name' => 'Footer widget 1',
		'description' => 'First footer widget area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'footer2',
		'name' => 'Footer widget 2',
		'description' => 'Second footer widget area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'footer3',
		'name' => 'Footer widget 3',
		'description' => 'Third footer widget area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
register_sidebar(array(
		'id' => 'footer4',
		'name' => 'Footer widget 4',
		'description' => 'Fourth footer widget area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
}
//register navigation
register_nav_menus( array(
 		'main' => __( 'Main Menu', 'ajbootstrap' ),
		'social' => __( 'Social Media Menu', 'ajbootstrap' ),
		'top' => __( 'Top Contact Menu', 'ajbootstrap' ),
		'corporate' => __( 'Corporat Footer Menu', 'ajbootstrap' )
 	) );
// Custom Backend Footer
function bigtex_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://bigtextrailers.com" target="_blank">Big Tex Trailers Marketing Team</a></span>.';
}
// adding it to the admin area
add_filter('admin_footer_text', 'bigtex_custom_admin_footer');
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
//enqueue styles and javascript
function stylin(){
	wp_enqueue_style('main', get_bloginfo( 'stylesheet_url', array(), '20140205', 'screen') );
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', '', null, 'screen');
	wp_enqueue_style('fonts', 'http://fonts.googleapis.com/css?family=Oswald:700|Roboto:400,700,900', '', '', 'screen');
	wp_enqueue_style('fontawesome', get_template_directory_uri().'/css/font-awesome.min.css', '', '', 'screen');
	wp_enqueue_style('magnific-pop', get_template_directory_uri().'/css/magnific-popup.css', '', '', 'screen');
}
add_action( 'wp_enqueue_scripts', 'stylin' );
function scriptaculous(){
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/javascript/bootstrap.min.js', '', '', true);
	wp_enqueue_script('custom', get_template_directory_uri().'/javascript/custom.js', '', '', true);
	wp_enqueue_script('mag-pop', get_template_directory_uri().'/javascript/magnific-popup.js', '', '', true);
}
add_action( 'wp_enqueue_scripts', 'scriptaculous' );
// add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
		echo '<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');
	//login logo on wp-login.php page. 200px x 100px png named site-login-logo.png in the images folder. Please create.
	function my_login_logo() { ?>
    <style type="text/css">
      body.login div#login h1 a{
			background-image:url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/site-login-logo.png);
			background-size:200px 100px;
			height:100px;
      padding-bottom:30px;
			width:200px;
      }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
//return blog url when clicking on the login logo on wp-login.php
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
//the title that appears when hovering over the login logo on wp-login.php. CHANGE THIS TO SUIT EACH DEALER
function my_login_logo_url_title() {
    return 'CM Truck Beds';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
//Favicon 16x16 png named favicon.png in images folder is the site's favicon. Please create.
/*function theme_favicon() { ?>
	<link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory') ?>/images/favicon.png" > 
<?php }*/
add_action('wp_head', 'theme_favicon');
// customize the wordpress visual editor
function my_mce_buttons( $buttons ){
	unset($buttons[7]);
	unset($buttons[8]);
	unset($buttons[9]);
  return $buttons;
}
add_filter('mce_buttons', 'my_mce_buttons');
//edit button on post and pages
function custom_edit_post_link($output) {
 $output = str_replace('class="post-edit-link"', 'class="post-edit-link edit-link btn btn-default btn-block uppercase"', $output);
 return $output;
}
add_filter('edit_post_link', 'custom_edit_post_link');
//Plugin Name: Custom Styles
//Plugin URI: http://www.speckygeek.com
//Description: Add custom styles in your posts and pages content using TinyMCE WYSIWYG editor. The plugin adds a Styles dropdown menu in the visual post editor.
//Based on TinyMCE Kit plug-in for WordPress
//http://plugins.svn.wordpress.org/tinymce-advanced/branches/tinymce-kit/tinymce-kit.php
///** Apply styles to the visual editor*/ 
add_filter('mce_css', 'tuts_mcekit_editor_style');
function tuts_mcekit_editor_style($url) {
	if ( !empty($url) )
			$url .= ',';
	// Retrieves the plugin directory URL
	// Change the path here if using different directories
	$url .= get_stylesheet_directory_uri() . '/css/editor-styles.css';
	return $url;
} 
add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' ); 
function tuts_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );
function tuts_mce_before_init( $settings ) {
	$style_formats = array(
		array(
			'title' => 'text-left',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-left'
		),
		array(
			'title' => 'text-right',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-right'
		),
		array(
			'title' => 'text-center',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-center'
		),
		array(
			'title' => 'text-justify',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'text-justify'
		),
		array(
			'title' => 'text-muted',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-muted',
		),
		array(
			'title' => 'text-primary',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-primary',
		),
		array(
			'title' => 'text-success',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-success',
		),
		array(
			'title' => 'text-info',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-info',
		),
		array(
			'title' => 'text-warning',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-warning',
		),
		array(
			'title' => 'text-danger',
			'selector' => 'p, td, th, div, ol, ol, li',
			'classes' => 'text-danger',
		),
		array(
			'title' => 'bg-primary',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-primary',
		),
		array(
			'title' => 'bg-success',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-success',
		),
		array(
			'title' => 'bg-info',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-info',
		),
		array(
			'title' => 'bg-warning',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-warning',
		),
		array(
			'title' => 'bg-danger',
			'selector' => 'p, a, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'bg-danger',
		),
		array(
			'title' => 'rightuppercase',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'rightuppercase',
		),
		array(
			'title' => 'leftuppercase',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'leftuppercase',
		),
		array(
			'title' => 'centeruppercase',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'centeruppercase',
		),
		array(
			'title' => 'pull-left',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'pull-left'
		),
		array(
			'title' => 'pull-right',
			'selector' => 'p, h1, h2, h3, h4, h5, h6, td, th, div, ol, ol, li, table, img',
			'classes' => 'pull-right'
		),
		array(
			'title' => 'img-rounded',
			'selector' => 'img',
			'classes' => 'img-rounded',
		),
		array(
			'title' => 'img-circle',
			'selector' => 'img',
			'classes' => 'img-circle',
		),
		array(
			'title' => 'img-thumbnail',
			'selector' => 'img',
			'classes' => 'img-thumbnail',
		),
		array(
			'title' => 'btn',
			'selector' => 'a, button',
			'classes' => 'btn',
		),
		array(
			'title' => 'btn-primary',
			'selector' => 'a, button',
			'classes' => 'btn-primary',
		),
		array(
			'title' => 'btn-success',
			'selector' => 'a, button',
			'classes' => 'btn-success',
		),
		array(
			'title' => 'btn-default',
			'selector' => 'a, button',
			'classes' => 'btn-default',
		),
		array(
			'title' => 'btn-info',
			'selector' => 'a, button',
			'classes' => 'btn btn-info',
		),
		array(
			'title' => 'btn-warning',
			'selector' => 'a, button',
			'classes' => 'btn-warning',
		),
		array(
			'title' => 'btn-danger',
			'selector' => 'a, button',
			'classes' => 'btn-danger',
		)		
	);
	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
} 
/*Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats*/
/*Add custom stylesheet to the website front-end with hook 'wp_enqueue_scripts'*/
add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue'); 
/*Enqueue stylesheet, if it exists.*/
function tuts_mcekit_editor_enqueue() {
  $StyleUrl = get_stylesheet_directory_uri().'/css/editor-styles.css'; // Customstyle.css is relative to the current file
  wp_enqueue_style( 'myCustomStyles', $StyleUrl );
}
//change default media linking to nothing
update_option('image_default_link_type','none');
//Add Theme Options menu item to Admin Bar
function custom_toolbar_link($wp_admin_bar) {
	$args = array(
			'id' => 'of_theme_options',
			'title' => __( 'Theme Options' ),
			'href' => admin_url( 'themes.php?page=options-framework' )
		);
	$wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'custom_toolbar_link', 999);
wp_enqueue_script('jquery');
?>