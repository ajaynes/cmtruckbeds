<div class="col-xs-12">
    <article>
        <h1 class="nomargin"><?php the_title(); ?></h1>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
        <?php endwhile; // end of the loop. ?>	
    </article>
</div><!--col-xs-8-->