<nav class="navbar navbar-default cm" role="navigation">
  <div class="navbar-header">
    <a class="navbar-brand" href="<?php bloginfo('url'); ?>/">
    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/cmTruckBedsLogo.png" class="img-responsive" alt="<?php bloginfo('name'); ?>" />
    </a>
  </div>
    <?php wp_nav_menu( array(
        'menu'      		=> 'main',
				'theme_location' => 'main',
        'depth'     		=> 2,
        'container'  		=> 'div',
        'container_class'   => 'hidden-xs hidden-sm',
        'container_id'      => 'bt-collapsing',
        'menu_class'        => 'nav navbar-nav cm',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      );
    ?>
</nav>