<div class="col-xs-12">
    <article>
    		 <h1 class="nomargin"><?php the_title(); ?></h1>
					<?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
          <?php endwhile; // end of the loop. ?>
    		<?php $queryCM = new WP_Query( 'cat=28' ); ?>
        <?php if ($queryCM->have_posts()) : ?>
        <?php while ( $queryCM->have_posts() ) : $queryCM->the_post(); ?>
        <div class="col-xs-12 col-sm-4">
        	<section class="singletruckbed">
							<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
							<?php the_content(); ?>
          <?php /*?><p><?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?></p><?php */?>
        	</section>
        </div>
        <?php endwhile; // end of the loop. ?>	
    </article>
    	<?php else : ?>
      <section class="post">
          <h2>Not Found</h2>
          <p>Sorry, but the requested resource was not found on this site.</p>
      </section>
  
      <?php endif; ?>
</div><!--col-xs-8-->