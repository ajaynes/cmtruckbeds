<div class="col-xs-12 col-md-8 col-md-push-4">
    <article>
        <h1><?php the_title(); ?></h1>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
        <?php endwhile; // end of the loop. ?>	
    </article>
</div><!--col-xs-8-->