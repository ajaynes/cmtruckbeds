<div class="col-xs-12 col-sm-8 col-sm-push-4">
    <article>
        <h1>Oh No! Page not found :(</h1>
        <div class="huge text-center">[404]</div>
        <p class="text-center">Please return to the <a href="<?php bloginfo('url'); ?>/">homepage</a>.</p>
    </article>
</div><!--span8-->