<?php
/*
Template Name: FB Landing Page
*/
?>
<?php get_header('fb'); ?>

<div class="container">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'home' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>