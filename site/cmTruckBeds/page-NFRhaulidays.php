<?php
/*
Template Name: NFR Haulidays Landing Page
*/
?>
<div class="nfr-white-body">    
    <?php get_template_part( '/inc/parts/content', 'headerhauliday' ); ?>    
    <div class="container">
    <img src="/wp-content/uploads/2016/11/nfr-haulidays-header.png" class="img-responsive center-block" alt="NFR Haulidays">
        <div class="mainholiday">
            <div class="content">
                <div class="row">
                     <?php get_template_part( '/inc/parts/content', 'hauliday' ); ?>    
                </div><!--row-->
            </div><!--content-->
        </div><!--mainholiday-->
    </div><!--container-->
</div><!--whitebody-->
<?php get_footer(); ?>