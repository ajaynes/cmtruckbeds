<section class="truckslider">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="trailerSlide">
          <div class="carousel slide" id="trailer">
            <div class="row">
              <div class="carousel-inner">
                <div class="item active">
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/er-truck-bed/">ER Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/er-truck-bed/">
                        <img src="http://cmtruckbeds.com/wp-content/uploads/2013/05/ER.png" class="img-responsive aligncenter" alt="ER Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/gp-truck-bed/">GP Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/gp-truck-bed/">
                        <img src="http://cmtruckbeds.com/wp-content/uploads/2013/05/gpthumb.png" class="img-responsive aligncenter" alt="GP Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/pl-truck-bed/">PL Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/pl-truck-bed/">
                        <img src="http://cmtruckbeds.com/wp-content/uploads/2013/05/plthumb.png" class="img-responsive aligncenter" alt="PL Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                </div><!--item-->
                <div class="item">
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/rd-truck-bed/">RD Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/rd-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/rdthumb.png" class="img-responsive aligncenter" alt="RD Model" />
                      </a>
                    </div><!--truck-->  
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/sk-truck-bed/">SK Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/sk-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skthumb.png" class="img-responsive aligncenter" alt="SK Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/aluminum-truck-beds/">Aluminum</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/aluminum-truck-beds/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/skalthumb.png" class="img-responsive aligncenter" alt="SK Aluminum Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                </div><!--item2-->
                <div class="item">
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/ss-truck-bed/">SS Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/ss-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/ssthumb.png" class="img-responsive aligncenter" alt="SS Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/tm-truck-bed/">TM Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/tm-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/tmthumb.png" class="img-responsive aligncenter" alt="TM Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/wd-truck-bed/">WD Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/wd-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/wdthumb.png" class="img-responsive aligncenter" alt="WD Model" />
                      </a>
                    </div><!--truck--> 
                  </div><!--col-xs-12 col-sm-4-->
                </div><!--item--> 
                <div class="item">
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/tmx-truck-bed/">TMX Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/tmx-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/tmxthumb.png" class="img-responsive aligncenter" alt="TMX Model" />
                      </a>
                    </div><!--truck-->  
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="http://cmtruckbeds.com/truck-beds/db-truck-bed/">DB Model</a>
                      </div><!--caption-->
                      <a href="http://cmtruckbeds.com/truck-beds/db-truck-bed/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/dbthumb.png" class="img-responsive aligncenter" alt="DB Model" />
                      </a>
                    </div><!--truck-->
                  </div><!--col-xs-12 col-sm-4-->
                  <div class="col-xs-4 text-center">
                  	<div class="truck">
                      <div class="caption">
                        <a href="truck-beds/tm-deluxe/">TM Deluxe</a>
                      </div><!--caption-->
                      <a href="truck-beds/tm-deluxe/">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/tmDeluxeThumb.png" class="img-responsive aligncenter" alt="TM Deluxe" />
                      </a>
                    </div><!--truck--> 
                  </div><!--col-xs-12 col-sm-4-->
                </div><!--item2-->
                
              </div><!--carousel-inner-->
            </div><!--row-->
            <a class="left carousel-control hidden-xs bt" href="#trailer" data-slide="prev">
              <span class="fa fa-chevron-left fa-2x left"></span>
            </a>
            <a class="right carousel-control hidden-xs bt" href="#trailer" data-slide="next">
              <span class="fa fa-chevron-right fa-2x right"></span>
            </a>
          </div><!--carousel slide-->
        </div><!--trailerSlide-->
      </div><!--col-xs-12-->
    </div><!--row-->
  </div><!--container-->
</section><!--truckslider-->