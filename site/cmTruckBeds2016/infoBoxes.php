<div class="row hidden-xs">
	<div class="col-sm-4">
  	<div class="infoBox">
    	<h3 class="text-center uppercase">Locate a Distributor</h3>
    	<p>Want to join the CM Truck Beds experience? Click here to find a distributor of one the nation's most recognized truck bed company. If it's the best you're looking for, we've got it, and want to help you find it.</p>
      <p><a href="http://cmtruckbeds.com/locate-a-distributor/" class="btn bt btn-info btn-lg btn-block uppercase">Locate a Distributor <i class="fa fa-long-arrow-right"></i></a></p>
    </div><!--infoBox-->
  </div><!--col-sm-4-->
  <div class="col-sm-4">
  	<div class="infoBox">
    	<h3 class="text-center uppercase">Warranty Information</h3>
    	<p>Along with our superior truck bed models, CM Truck Beds also offers warranty as part of our exceptional CM experience. For more information and details about CM Truck Beds warranty, follow this link.</p>
      <p><a href="http://cmtruckbeds.com/warranty-center/" class="btn bt btn-info btn-lg btn-block uppercase">Learn More <i class="fa fa-long-arrow-right"></i></a></p>
    </div><!--infoBox-->
  </div><!--col-sm-4-->
  <div class="col-sm-4">
  	<div class="infoBox">
    	<h3 class="text-center uppercase">Download Brochure</h3>
    	<p>Want to know the ins and outs of our truck bed models? Download one of our brochures today and get in the know about our professional grade, heavy duty work trucks.</p><br>
      <p><a href="http://cmtruckbeds.com/wp-content/uploads/2016/02/CMTruckBedsBrochure.pdf" target="_blank" class="btn bt btn-info btn-lg btn-block uppercase">Download a Brochure <i class="fa fa-long-arrow-right"></i></a></p>
    </div><!--infoBox-->
  </div><!--col-sm-4-->
</div><!--row-->