<div class="col-xs-12">
<article>
  <?php /*?><h1><?php the_title(); ?></h1><?php */?>
  <?php while ( have_posts() ) : the_post(); ?>
  	<?php the_content(); ?>
  	<?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
  <?php endwhile; // end of the loop. ?> 
</article>
</div>
