<section class="truckbeds">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
      <div class="row"><div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center"><h1><?php the_title(); ?></h1></div></div>
        <?php the_content(); ?>
        <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
    <?php endwhile; // end of the loop. ?>
  </div>	
</section><!--trucbeds-->