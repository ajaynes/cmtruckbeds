<section class="truckbeds">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
      <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
    <?php endwhile; // end of the loop. ?>
  </div>	
</section><!--trucbeds-->