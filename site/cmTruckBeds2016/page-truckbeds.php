<?php
/*
Template Name: All Truck Beds
*/
?>
<?php get_header(); ?>
<div class="container">
<div class="content">
  <?php get_template_part( '/inc/parts/content', 'truckbeds' ); ?>
</div><!--content-->
</div><!--container-->
<section class="buttons"><div class="container"><?php get_template_part( 'featuredbuttons' ); ?></div></section>
<div class="container">
<?php get_footer(); ?>