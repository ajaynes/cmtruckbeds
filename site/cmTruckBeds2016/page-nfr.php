<?php
/*
Template Name: NFR Page
*/
?>
<?php get_header('nfr'); ?>
<div>
<?php putRevSlider("nfr1") ?>
</div>
<div class="nfrUp">
<div class="container">
<div class="row nfrFeat">
    <div class="col-xs-12 col-md-4"><img src="/wp-content/uploads/2016/07/sk.png"/></div>
    <div class="col-xs-12 col-md-4"><img src="/wp-content/uploads/2016/07/rd.png"/></div>
    <div class="col-xs-12 col-md-4"><img src="/wp-content/uploads/2016/07/tmx.png"/></div>
</div>
</div>
</div>
<?php putRevSlider("nfr2") ?>
<div class="nfrLow">
<div class="container">
<div class="row nfrAbout">
    <div class="col-xs-12 col-md-6"><img src="/wp-content/uploads/2016/07/about-video.png"/></div>
    <div class="col-xs-12 col-md-6"><img src="/wp-content/uploads/2016/07/event-location-maps-etc.png"/></div>
</div>
<div class="row nfrBull">
    <div class="col-xs-12 col-md-4"><img src="/wp-content/uploads/2016/07/featured-productsCM.png"/></div>
    <div class="col-xs-12 col-md-4"><img src="/wp-content/uploads/2016/07/heith-demoss.png"/></div>
    <div class="col-xs-12 col-md-4"><img src="/wp-content/uploads/2016/07/sarah-mcdonald.png"/></div>
</div>
</div>
</div>
<div class="container">
<div class="homepage">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'home' ); ?>
  </div><!--row-->
  </div>
  </div>
<?php get_footer(); ?>