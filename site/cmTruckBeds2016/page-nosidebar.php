<?php
/*
Template Name: Fullwidth No sidebar
*/
?>
<?php get_header(); ?>
<div class="container">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'fullwidth' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>